# Setux REPL

Rudimentary REPL / CLI for [Setux]

[PyPI] - [Repo] - [Doc]


[PyPI]: https://pypi.org/project/setux_repl
[Repo]: https://framagit.org/louis-riviere-xyz/setux_repl
[Doc]: https://setux-repl.readthedocs.io
[Setux]: https://setux.readthedocs.io
