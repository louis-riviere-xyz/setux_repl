# Setux REPL

Rudimentary REPL / CLI for [Setux]

[PyPI] - [Repo] - [Doc]

## Usage

### REPL

```bash
$ setux [target]
```

### CLI

```bash
$ setux [target] command
```

## Installation

```bash
$ pip install setux_repl
```


[PyPI]: https://pypi.org/project/setux_repl
[Repo]: https://framagit.org/louis-riviere-xyz/setux_repl
[Doc]: https://setux-repl.readthedocs.io
[Setux]: https://setux.readthedocs.io
