# Commands
  
## actions  
**List Actions**  
      
## commands  
**List commands**  
      
## config  
**Setux Configuration.**  
      
    Edit the Setux Config file.  
      
## doc  
**Setux Documentation.**  
      
    Open the main Setux Doc page in the default browser.  
      
## edit  
**Edit a file**  
      
## help  
**Show Help.**  
      
## logs  
**Show log files.**  
      
## managers  
**List managers.**  
      
## mappings  
**List mappings.**  
      
## modules  
**List modules.**  
      
## outlog  
**Show commands logs.**  
      
## outrun  
**Show commands history.**  
      
## sh  
**Run shell cmd on target.**  
      
## sudo  
**Run shell cmd as root.**  
